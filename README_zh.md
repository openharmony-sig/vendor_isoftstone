# vendor_isoftstone

## 介绍

该仓库托管isoftstone产品：扬帆开发板、致远开发板、乘风1000开发板和九霄开发板的配置参数。

“扬帆”装载瑞芯微RK3399芯片，基于Big.Little架构，其丰富的扩展接口可实现LCD显示、触摸、多媒体、上网等基本特性，可广泛应用于互动广告机、互动数字标牌、智能自助终端、智能零售终端、工控主机、机器人设备等各类场景。

“致远”搭载全志T507芯片，基于ARM架构，四核64位处理器，支持蓝牙、WIFI、音频、视频和摄像头等功能。拥有丰富的扩展接口，以及多种视频输入输出接口； 适用于工业控制、智能驾舱、智慧家居、智慧电力、在线教育等诸多行业需求。

“乘风1000”搭载龙芯2K1000芯片，处理器基于LoongArch架构， LA264双核 处理器，主频高达1GHz。板载内存2G。拥有丰富的扩展接口，网卡，PCIE，CAN ，USB，串口；支持两种视频输入输出接口：HDMI，DVO；支持3.5mm标准音频输入输出接口；适用于政企服务器、工业控制、金融、交通等敏感行业。

“九霄"搭载的是intel495芯片，系统适配到OpenHarmony Release 4.0上，硬件适配到戴尔（DELL） 3400 I7 16G SSD-256G 独显2G笔记本上。

## 目录

```
vendor/isoftstone/                  # vendor_isoftstone 仓库路径
├── README.md                       # README文档
└── yangfan                         # 扬帆相关配置
    ├── hals                        # hal层相关配置
    └── hdf_config                  # hdf相关配置
└── zhiyuan                         # 致远相关配置	
    ├── hals                        # hal层相关配置
    └── hdf_config                  # hdf相关配置
└── chengfeng1000                   # 乘风1000相关配置	
    ├── hals                        # hal层相关配置
    └── hdf_config                  # hdf相关配置
└── jiuxiao                         # 九霄相关配置	
    ├── applications                # applications目录中源码修改
    ├── build                       # build目录中源码修改
    ├── config.json                 # jiuxiao单板相关配置
    ├── default_app_config          # app相关配置
    ├── etc                         # etc相关配置
    ├── hdf_config                  # hdf相关配置
    ├── install                     # 安装说明
    ├── intel495.sh                 # 合入patch脚本
    ├── ohos.build                  # 需要编译的相关配置
    ├── patch                       # 存放修改patch
    ├── power_config                # power相关配置
    ├── preinstall-config           # preinstall相关配置
    ├── product.gni                 # 编译相关配置
    ├── resourceschedule            # resourceschedule相关配置
    └── security_config             # security相关配置
```

## 开发环境搭建

扬帆使用参考 [使用教程](https://gitee.com/openharmony/device_board_isoftstone/blob/master/yangfan/README_zh.md)

致远使用参考 [使用教程](https://gitee.com/openharmony/device_board_isoftstone/blob/master/zhiyuan/README_zh.md)

乘风1000使用参考 [使用教程](https://gitee.com/openharmony-sig/device_board_isoftstone)

九霄开发板使用参考 [使用教程](https://gitee.com/openharmony-sig/device_board_intel/blob/master/intel495/README_zh.md)


## 贡献

[如何参与](https://gitee.com/openharmony/docs/blob/HEAD/zh-cn/contribute/%E5%8F%82%E4%B8%8E%E8%B4%A1%E7%8C%AE.md)

[Commit message规范](https://gitee.com/openharmony/device_qemu/wikis/Commit%20message%E8%A7%84%E8%8C%83?sort_id=4042860)

## 相关仓

* [device_board_isoftstone](https://gitee.com/openharmony/device_board_isoftstone)
* [device_board_intel](https://gitee.com/openharmony-sig/device_board_intel)


