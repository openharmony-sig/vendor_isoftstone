# vendor_isoftstone

## 介绍
在vendor/isoftstone/chengfeng1000/目录下，以龙芯2K1000芯片作为开发板主控器的开发板样例。

## 软件架构
代码路径说明

```
vendor/isoftstone/chengfeng1000/
└── graphic_config   
└── hals
└── hdf_config   
└── init_config   
```

## 使用说明

### 编译

```
进入device/board/isoftstone/chengfeng1000目录参考REAMD.md文档进行编译
```

## 参与贡献

[如何贡献](https://gitee.com/openharmony/docs/blob/HEAD/zh-cn/contribute/%E5%8F%82%E4%B8%8E%E8%B4%A1%E7%8C%AE.md)

[Commit message规范](https://gitee.com/openharmony/device_qemu/wikis/Commit%20message%E8%A7%84%E8%8C%83)

## 相关仓

[device/soc/sioftstone](https://gitee.com/openharmony-sig/device_soc_loongson)

[device/board/isoftstone](https://gitee.com/openharmony-sig/device_board_isoftstone)

