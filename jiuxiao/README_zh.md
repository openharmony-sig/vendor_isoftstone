# vendor_isoftstone

## 介绍
在vendor/isoftstone/jiuxiao/目录下，以Intel(R) Core(TM) i7-8565U CPU芯片使用在DELL笔记本上作为样例。

## 软件架构
代码路径说明

```
vendor/isoftstone/jiuxiao/
├── applications                # applications目录中源码修改
├── build                       # build目录中源码修改
├── config.json                 # jiuxiao单板相关配置
├── default_app_config          # app相关配置
├── etc                         # etc相关配置
├── hdf_config                  # hdf相关配置
├── install                     # 安装说明
├── intel495.sh                 # 合入patch脚本
├── ohos.build                  # 需要编译的相关配置
├── patch                       # 存放修改patch
├── power_config                # power相关配置
├── preinstall-config           # preinstall相关配置
├── product.gni                 # 编译相关配置
├── resourceschedule            # resourceschedule相关配置
└── security_config             # security相关配置
```

## 使用说明

### 编译

```
进入device/board/intel/intel495目录参考README_zh.md文档进行编译
```

## 参与贡献

[如何贡献](https://gitee.com/openharmony/docs/blob/HEAD/zh-cn/contribute/%E5%8F%82%E4%B8%8E%E8%B4%A1%E7%8C%AE.md)

[Commit message规范](https://gitee.com/openharmony/device_qemu/wikis/Commit%20message%E8%A7%84%E8%8C%83)

## 相关仓

[device/soc/intel](https://gitee.com/openharmony-sig/device_soc_intel)

[device/board/intel](https://gitee.com/openharmony-sig/device_board_intel)

