# Copyright (c) 2024 SwanLink (Jiangsu) Technology Development Co., LTD.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/bin/bash

proj_info_all=`repo forall -c 'echo $REPO_PATH:$REPO_PROJECT'`
current_code_path=`pwd`

for proj_info in ${proj_info_all};
    do
		 cd  ${current_code_path}/${proj_info%:*}
	     git remote set-url origin http://10.61.197.48:8089/${proj_info#*:}
    done
	
echo "replace remote url success"
	
cd  ${current_code_path}	
find ${current_code_path}/.repo -name "*.xml" -exec sed -i "s/10.28.211.22:8090/10.61.197.48:8089/g" {} \;
find ${current_code_path}/.repo -name "config" -exec sed -i "s/10.28.211.22:8090/10.61.197.48:8089/g" {} \;
find ${current_code_path}/.repo -name "FETCH_HEAD" -exec sed -i "s/10.28.211.22:8090/10.61.197.48:8089/g" {} \;
find ${current_code_path}/.repo -name ".repo_config.json" -exec sed -i "s/10.28.211.22:8090/10.61.197.48:8089/g" {} \;

echo "repo forall -c 'git lfs pull'"	
repo forall -c 'git lfs pull'	
	
	










