# Copyright (c) 2024 SwanLink (Jiangsu) Technology Development Co., LTD.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/bin/bash

CURRENT_DIR=$(pwd)
SOURCE_ROOT_DIR=$CURRENT_DIR/../../

cd $SOURCE_ROOT_DIR/applications/standard/permission_manager/
patch -p1 < $CURRENT_DIR/patch/applications_authority-management.patch

cd $SOURCE_ROOT_DIR/applications/standard/hap/
patch -p1 < $CURRENT_DIR/patch/applications_hap_build.patch

cd $SOURCE_ROOT_DIR//arkcompiler/ets_runtime/
patch -p1 < $CURRENT_DIR/patch/arkcompiler_ets_runtime_build.patch

cd $SOURCE_ROOT_DIR/base/hiviewdfx/faultloggerd/
patch -p1 < $CURRENT_DIR/patch/base_hiviewdfx_faultloggerd.patch

cd $SOURCE_ROOT_DIR/base/hiviewdfx/hilog/
patch -p1 < $CURRENT_DIR/patch/base_hiviewdfx_hilog.patch

cd $SOURCE_ROOT_DIR/base/hiviewdfx/hisysevent/
patch -p1 < $CURRENT_DIR/patch/base_hiviewdfx_hisysevent.patch

cd $SOURCE_ROOT_DIR/base/hiviewdfx/hitrace/
patch -p1 < $CURRENT_DIR/patch/base_hiviewdfx_hitrace.patch

cd $SOURCE_ROOT_DIR/base/security/appverify/
patch -p1 < $CURRENT_DIR/patch/base_security_appverify.patch

cd $SOURCE_ROOT_DIR/base/security/code_signature/
patch -p1 < $CURRENT_DIR/patch/base_security_code_signature.patch

cd $SOURCE_ROOT_DIR/base/startup/init/
patch -p1 < $CURRENT_DIR/patch/base_startup_init.patch

cd $SOURCE_ROOT_DIR//base/useriam/fingerprint_auth/
patch -p1 < $CURRENT_DIR/patch/base_useriam_fingerprint_auth.patch

cd $SOURCE_ROOT_DIR/build/
patch -p1 < $CURRENT_DIR/patch/build_config.patch

cd $SOURCE_ROOT_DIR/developtools/hdc/
patch -p1 < $CURRENT_DIR/patch/developtools_hdc.patch

cd $SOURCE_ROOT_DIR/developtools/profiler/
patch -p1 < $CURRENT_DIR/patch/developtools_profiler.patch

cd $SOURCE_ROOT_DIR/foundation/ability/ability_runtime/
patch -p1 < $CURRENT_DIR/patch/foundation_ability_ability_runtime.patch

cd $SOURCE_ROOT_DIR/foundation/arkui/ace_engine/
patch -p1 < $CURRENT_DIR/patch/foundation_arkui_ace_engine.patch

cd $SOURCE_ROOT_DIR/foundation/arkui/napi/
patch -p1 < $CURRENT_DIR/patch/foundation_arkui_napi.patch

cd $SOURCE_ROOT_DIR/foundation/bundlemanager/bundle_framework/
patch -p1 < $CURRENT_DIR/patch/foundation_bundlemanager_bundle_framework.patch

cd $SOURCE_ROOT_DIR/foundation/communication/dsoftbus/
patch -p1 < $CURRENT_DIR/patch/foundation_communication_dsoftbus.patch

cd $SOURCE_ROOT_DIR/foundation/communication/netmanager_base/
patch -p1 < $CURRENT_DIR/patch/foundation_communication_netmanager_base.patch

cd $SOURCE_ROOT_DIR/foundation/filemanagement/storage_service/
patch -p1 < $CURRENT_DIR/patch/foundation_filemanagement_storage_service.patch

cd $SOURCE_ROOT_DIR/foundation/graphic/graphic_2d/
patch -p1 < $CURRENT_DIR/patch/foundation_graphic_graphic_2d.patch

cd $SOURCE_ROOT_DIR/foundation/multimodalinput/input/
patch -p1 < $CURRENT_DIR/patch/foundation_multimodalinput_input.patch

cd $SOURCE_ROOT_DIR/foundation/resourceschedule/device_standby/
patch -p1 < $CURRENT_DIR/patch/foundation_resourceschedule_device_standby.patch

cd $SOURCE_ROOT_DIR/foundation/resourceschedule/ffrt/
patch -p1 < $CURRENT_DIR/patch/foundation_resourceschedule_ffrt.patch

cd $SOURCE_ROOT_DIR//foundation/systemabilitymgr/samgr/
patch -p1 < $CURRENT_DIR/patch/foundation_systemabilitymgr_samgr.patch

cd $SOURCE_ROOT_DIR/foundation/window/window_manager/
patch -p1 < $CURRENT_DIR/patch/foundation_window_manager.patch

cd $SOURCE_ROOT_DIR//test/testfwk/xdevice/
patch -p1 < $CURRENT_DIR/patch/test_testfwk_xdevice.patch

cd $SOURCE_ROOT_DIR/test/xts/acts/
patch -p1 < $CURRENT_DIR/patch/test_xts_acts.patch

cd $SOURCE_ROOT_DIR/third_party/flutter/
patch -p1 < $CURRENT_DIR/patch/third_party_flutter.patch

cd $SOURCE_ROOT_DIR/third_party/libdrm/
patch -p1 < $CURRENT_DIR/patch/third_party_libdrm.patch

cd $SOURCE_ROOT_DIR/third_party/libunwind/
patch -p1 < $CURRENT_DIR/patch/third_party_libunwind.patch

cd $SOURCE_ROOT_DIR/third_party/musl/
patch -p1 < $CURRENT_DIR/patch/third_party_musl.patch

cd $SOURCE_ROOT_DIR/third_party/node/
patch -p1 < $CURRENT_DIR/patch/third_party_node.patch

cd $SOURCE_ROOT_DIR/drivers/interface/
patch -p1 < $CURRENT_DIR/patch/drivers_interface_display.patch

cd $SOURCE_ROOT_DIR/drivers/peripheral/
patch -p1 < $CURRENT_DIR/patch/drivers_peripheral_base.patch

cd $SOURCE_ROOT_DIR/drivers/hdf_core/
patch -p1 < $CURRENT_DIR/patch/drivers_hdf_core.patch

cd $SOURCE_ROOT_DIR/productdefine/common/
patch -p1 < $CURRENT_DIR/patch/productdefine_common.patch

cp -rf $CURRENT_DIR/applications $SOURCE_ROOT_DIR/

cp -rf $CURRENT_DIR/build $SOURCE_ROOT_DIR/

mkdir -p $SOURCE_ROOT_DIR/third_party/FreeBSD/lib/libc/amd64
cp -f $SOURCE_ROOT_DIR/third_party/FreeBSD/lib/libc/arm/gd_qnan.h $SOURCE_ROOT_DIR/third_party/FreeBSD/lib/libc/amd64
cp -f $SOURCE_ROOT_DIR/third_party/FreeBSD/lib/libc/aarch64/arith.h $SOURCE_ROOT_DIR/third_party/FreeBSD/lib/libc/amd64
