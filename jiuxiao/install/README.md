[toc]





## 前置条件

1. PC机（**BIOS**打开**Secure Boot**的， 需要关闭， 否则启动OHOS时， 提示"**bad shim signature**")
2. ubuntu-22.04.1-desktop-amd64.iso 制作的U盘安装盘

## 安装ubuntu

### 安装

1.  拔掉网线(不带网络安装), 插上U盘安装盘, 上电

2. `Welcome`界面选择`Install Ubuntu`

3. 'Keyboard layout'选择`Continue`

4. `Updates and other software`界面选择`Minimal install`, 其他选择都不选择

   ![01_install_ubuntu_03](resource\images\01_install_ubuntu_03.png)

5. `Installation type`选择`Something else`

   ![01_install_ubuntu_04](resource\images\01_install_ubuntu_04.png)

6. 分区

      1. (可能需要)
      
         ![image-20231011111244841](resource\images\install_001.png)
      
      2. EFI分区128MB

![01_install_ubuntu_06_create_efi_partion](resource\images\01_install_ubuntu_06_create_efi_partion.png)

      3. 创建root分区(建议40GB以上, 并给ohos x86留下20GB, x86 ohos系统中system 10GB以上, 其他的都可以小一点)

![01_install_ubuntu_07_create_root_partion](resource\images\01_install_ubuntu_07_create_root_partion.png)
    
      4. 选择root分区安装, 点击`Install Now`

![01_install_ubuntu_08_select_root_partion_to_install](resource\images\01_install_ubuntu_08_select_root_partion_to_install.png)
      
![01_install_ubuntu_09_install](resource\images\01_install_ubuntu_09_install.png)
    

         7. 选择时区
    
         8. 选择`What are you?`




![01_install_ubuntu_11_install_progress](resource\images\01_install_ubuntu_12.png)



![01_install_ubuntu_11_install_progress](resource\images\01_install_ubuntu_11_install_progress.png)
    

### 配置ubuntu

      > 1) 安装ubuntu后, 进入桌面系统, 可能会打不开终端, 可以按<kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>F3/<kbd>组合键, 切换到文本控制台, 执行`localectl set-locale en_US.UTF-8`。 然后再切换到图形界面就可以打开终端。
    
      1. 移除安装U盘
    
      2. 配置ip
      > 如果网络中有DHCP, 则可以自动获取到IP.
    
      3. 进入ubuntu后安装软件
    
         1) apt udpate
    
         2) apt install openssh-server gparted vim net-tools
    
      4. 配置sshd
    
         修改 /etc/ssh/sshd_config 中增加 PermitRootLogin yes
         重启sshd服务    
         service sshd restart
    
      > 至此, 后面的操作, 你可以使用ssh的方式操作.
    
      5. 分区
    
         查询硬盘与分区信息
    
         `lsblk --tree --sort TYPE`
    
         fdisk -l /dev/sdb
         Disk /dev/sdb: 100 GiB, 107374182400 bytes, 209715200 sectors
         Disk model: VBOX HARDDISK
         Units: sectors of 1 * 512 = 512 bytes
         Sector size (logical/physical): 512 bytes / 512 bytes
         I/O size (minimum/optimal): 512 bytes / 512 bytes
    
         自动分区
    
         https://cloud.tencent.com/developer/article/2322927
    
         https://blog.csdn.net/weixin_34290352/article/details/92756757



### 分区信息

  **Table** 分区信息

| 分区名称(Partion name) | 挂载点      | 分区格式     | 最小大小     | 最小分区MB显示 | 建议大小 |
| --------  | --------     | -------   | --------     | ------- | ------- |
| system    | /usr         | ext4      | 10GB         | 10240 | fixed   |
| vendor    | /vendor      | ext4      | 1GB          | 1024 | fixed   |
| data      | /data        | ext4      | 5GB          | 5120 | 取最大值 |
| misc      | /misc        | cleaned   | 100MB        | 100 | fixed   |
| sys_prod  | /sys_prod    | ext4      | 100MB        | 100 | fixed   |
| chip_prod | /chip_prod   | ext4      | 100MB        | 100 | fixed   |

需要磁盘空间至少为17GB以上.

### 使用gparted分区
ubuntu图形

#### 新建system分区

![image-20231011143340164](resource\images\20_create_partion.png)



![image-20231011143519374](resource\images\install_010.png)



#### 新建misc分区

![image-20231011143643720](resource\images\install_020.png)



#### 分区创建好后点击`menu`->`Edit`->`Apply All Operations`

![image-20231011143836721](resource\images\install_030.png)



#### 创建好的分区与下图类似

![image-20231011144056883](resource\images\install_040.png)

### 查询分区信息

```
lsblk --tree -o NAME,PATH,SIZE,TYPE,partlabel,fstype
```

![image-20231011172826387](resource\images\install_050.png)



## 修改串口

1) 修改`/etc/default/grub`

```
vim /etc/default/grub
```



```
GRUB_TIMEOUT=10
GRUB_CMDLINE_LINUX="console=tty0 console=ttyS0,115200n8"
GRUB_SERIAL_COMMAND="serial --unit=0 --speed=115200 --word=8 --parity=no --stop=1"
GRUB_TERMINAL="console serial"
```

2) 执行` update-grub2`

```
update-grub2
```

![image-20231008120052183](resource\images\update-grub2.png)\

3) 修改`/boot/grub/grub.cfg`中的timeout

```
vim /boot/grub/grub.cfg
```

![image-20231008121013068](resource\images\modify_boot_grub_grub_cfg_timeout.png)

4) 重启后, 就可以从串口看到启动日志, 并可以登录与调试

## 启动菜单

###  修改文件

```
vim /boot/grub/grub.cfg
```

### 拷贝一个`menuentry 'Ubuntu' --class ubuntu`
```
menuentry 'OHOS' --class ubuntu --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple-b1197514-734a-46a2-a34b-2b81341861e6' {
	recordfail
	load_video
	gfxmode $linux_gfx_mode
	insmod gzio
	if [ x$grub_platform = xxen ]; then insmod xzio; insmod lzopio; fi
	insmod part_gpt
	insmod ext2
	set root='hd0,gpt2'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,gpt2 --hint-efi=hd0,gpt2 --hint-baremetal=ahci0,gpt2  b1197514-734a-46a2-a34b-2b81341861e6
	else
	  search --no-floppy --fs-uuid --set=root b1197514-734a-46a2-a34b-2b81341861e6
	fi
	linux	/boot/bzImage init=/bin/init console=ttyS0,115200 hardware=jiuxiao rw selinux=0 vga=0x035c ohos.required_mount.system=/dev/block/sda3@/usr@ext4@ro,barrier=1@wait,required ohos.required_mount.vendor=/dev/block/sda4@/vendor@ext4@ro,barrier=1@wait,required ohos.required_mount.misc=/dev/block/sda6@/misc@none@none@wait,required
	initrd	/boot/ramdisk.img
}
```

### 修改点diff文件

```diff
136c136
< menuentry 'Ubuntu' --class ubuntu --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple-93051b3b-ea44-440e-92d7-9cf8faefea23' {
---
> menuentry 'OHOS' --class ubuntu --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple-93051b3b-ea44-440e-92d7-9cf8faefea23' {
150,151c150,151
<       linux   /boot/vmlinuz-5.15.0-43-generic root=UUID=93051b3b-ea44-440e-92d7-9cf8faefea23 ro console=tty0 console=ttyS0,115200n8 quiet splash $vt_handoff
<       initrd  /boot/initrd.img-5.15.0-43-generic
---
>         linux /boot/bzImage init=/bin/init console=ttyS0,115200 hardware=jiuxiao rw selinux=0 vga=0x035c ohos.required_mount.system=/dev/block/sda4@/usr@ext4@ro,barrier=1@wait,required ohos.required_mount.vendor=/dev/block/sda5@/vendor@ext4@ro,barrier=1@wait,required ohos.required_mount.misc=/dev/block/sda7@/misc@none@none@wait,required
>       initrd  /boot/ramdisk.img
```

## 编译机与PC互信设置

```
ssh-copy-id -i ~/.ssh/id_rsa.pub root@10.10.10.10
```

![image-20231008122702593](resource\images\install_pc_ssh_inter_x86_pc.png)



## 安装

### 拷贝ohos x86镜像

```
ssh-copy-id -i ~/.ssh/id_rsa.pub root@10.10.10.10
```



> FAQ:
>
> 如果原来安装过sshkey, 则加-f
>
> ssh-copy-id  -f -i ~/.ssh/id_rsa.pub root@10.10.10.10



### 执行安装脚本

```
bash vendor/isoftstone/jiuxiao/install/x86_install.sh 
```

### help

```
    help:
    Usage: ${script_name} [ --local_pc | --ip <target_pc_ip> ] [[ --image_path | -i <image_path> ] | [ -t mount_type ] [ -o unc_path_mount_options ] [ --code_path | -c <code_path> ] [ --disable_reboot | -r ] [ --quiet | -q ] [ --debug | -d ]

    --local_pc | -l                 install local pc
    --ip ip                         target pc ip
    --image_path | -i image_path    image path, storing files such as system.img, vendor.img, bzImage, and etc.
                                    !!! ATTENTION !!! if image_path is a windows unc path, please quoted with single quote.
    -t mount_type                   mount_type, e.g. cifs
    -o unc_path_mount_options       mount options for unc path mount, e.g.
                                    1) windows path: e.g. -o username=root,password=123456
                                    2) linux path: There is no adaptation
    --code_path | -c code_path      developer code path, which is repo home.
    --disable_reboot | -r           do not reboot when image installed
    --quiet | -q                    quiet execute, no answer
    --debug | -d                    debug mode
    example:
    sudo bash ${script_name} --local_pc --code_path /home/code
    sudo bash ${script_name} --ip 10.10.10.10 --image_path '\\\\10.10.10.1\\daily_build\\image' -o username=root,password=123456
    sudo bash ${script_name} --ip 10.10.10.10 --image_path /home/code/out/jiuxiao/packages/phone/images
    sudo bash ${script_name} --ip 10.10.10.10 --code_path /home/code
```



# FAQ

## 常见的机器进入BIOS的方法

| 笔记本型号   | 操作方法 | BIOS安全选项位置 | 是否有板板载串口 |
| ------------ | -------- | ---------------------------- | ----------------------------- |
| HP288 台式机 | F10      | 启动选项->安全启动模式：禁用 | 无板载串口， 不能使用串口调试 |
| 启天420      | ESC      | 安全菜单->安全启动：关闭<br> | 有1个板载串口 |
| 戴尔（DELL） 3400 I7 16G SSD-256G 独显2G | F12 |      | 无板载串口 |

## 离线方式安装包

```

```



```
mkdir /home/hhwl/test
tar xf test.tar.gz -C /home/hhwl/test

```







